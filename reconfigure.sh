rm -rf ~/.pub-cache/
fvm flutter pub cache repair
find . -type f -name '*.g.dart' -print -delete && find . -type f -name 'k.freezed.dart' -print -delete
fvm flutter clean
rm pubspec.lock
fvm flutter pub get
fvm flutter packages pub run build_runner build --delete-conflicting-outputs